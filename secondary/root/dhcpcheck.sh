#!/bin/sh

PRIMARY_DHCP=192.168.1.1

rsync --daemon

while true; do

current_state=$(uci get dhcp.lan.ignore)
if [ -z "$current_state" ]
then
        current_state=0
fi

ping -c1 -q -w1 $PRIMARY_DHCP >/dev/null
if [ $? -eq 0 ]
then
        new_state=1
#       echo "Disable LAN DHCP"
else
        new_state=0
#       echo "Enable LAN DHCP"
fi

if [ "$current_state" -ne "$new_state" ]
then
        logger "Restarting DCHP ($new_state)"
        uci set dhcp.lan.ignore=$new_state
        uci commit dhcp
        mv /tmp/dhcp.leases_remote /tmp/dhcp.leases
        /etc/init.d/dnsmasq restart
fi

sleep 5

done
